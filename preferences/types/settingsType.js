const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Types = Extension.imports.preferences.types;
const Utils = Extension.imports.libs.utils;

/* exported SettingsType */
var SettingsType = class {
    get_widget() {
        return 'unknown';
    }

    get_row() {
        return this.get_widget();
    }

    get_signal() {
        return null;
    }

    get_value_from_widget(ignored_object) {
        return null;
    }

    update_widget(ignored_widget, ignored_setting_value) {
    }

    validate(ignored_setting, ignored_setting_data) {
        return [true, ''];
    }

    extra(ignored_params) {
    }

    additional_templates() {
        return [];
    }
};

/* exported getType */
function getType(data) {
    if (Utils.isChildValid(data, 'widget')) {
        if (data['widget'] == 'spin') {
            return new Types.spinType.SpinType();
        }
        if (data['widget'] == 'color') {
            return new Types.colorType.ColorType();
        }
        if (data['widget'] == 'priorityMarkup') {
            return new Types.priorityMarkupType.PriorityMarkupType();
        }
    }
    if (data['type'] == 'boolean') {
        return new Types.booleanType.BooleanType();
    }
    if (data['type'] == 'integer') {
        return new Types.integerType.IntegerType();
    }
    if (data['type'] == 'string') {
        return new Types.stringType.StringType();
    }
    if (data['type'] == 'path') {
        return new Types.pathType.PathType();
    }
    if (data['type'] == 'help') {
        return new Types.helpType.HelpType();
    }
    if (data['type'] == 'shortcut') {
        return new Types.shortcutType.ShortcutType();
    }
    return null;
}

