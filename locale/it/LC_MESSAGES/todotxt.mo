��          �            h     i     �  #   �     �     �     �  ,   �     	          (     +     9  !   H  )   j  �  �     d     x  B   �     �     �     �  *      $   +     P     c     f     x  !   �  :   �                                               	             
                    %(file) exists already Archive %(task) Auto-add creation date to new tasks Cancel Cannot open file Create new file Create todo.txt and done.txt file in %(path) Error writing file New task… Ok Open settings Open task list Open todo.txt file in text editor Unknown error during file write: %(error) Project-Id-Version: Italian (todo-txt-gnome-shell-extension)
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: Italian <https://hosted.weblate.org/projects/todo-txt-gnome-shell-extension/extension-and-preferences/it/>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.14-dev
 %(file) esiste già Archivia %(task) Aggiungi automaticamente la data di creazione alle nuove attività Cancella Impossibile aprire il file Crea nuovo file Crea i file todo.txt e done.txt in %(path) Errore durante la scrittura del file Nuova attività… Ok Apri Impostazioni Apri lista delle attività Apri il file todo.txt nell'editor Errore sconosciuto durante la scrittura del file: %(error) 