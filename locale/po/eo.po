# todo.txt gnome-shell extension translation file
#
# This file is distributed under the same license as the todo.txt gnome-shell extension package.
#
# Translators:
# phlostically <phlostically@mailinator.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: todo.txt gnome-shell extension\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-05 15:43+0200\n"
"PO-Revision-Date: 2022-10-23 20:08+0000\n"
"Last-Translator: phlostically <phlostically@mailinator.com>\n"
"Language-Team: Esperanto <https://hosted.weblate.org/projects/"
"todo-txt-gnome-shell-extension/extension-and-preferences/eo/>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.14.2-dev\n"

#: ../libs/errors.js:28
msgid "%(file) cannot be written. Please check its permissions."
msgstr ""

#: ../libs/todoTxtManager.js:524
msgid "%(file) exists already"
msgstr ""

#: ../libs/openTextEditorEntry.js:23
msgid ""
"An error occured while trying to launch the default text editor: %(error)"
msgstr ""

#: ../libs/errors.js:37
msgid "An error occured while writing to %(file): %(error)"
msgstr ""

#: ../libs/todoMenuItem.js:258
msgid "Archive %(task)"
msgstr "Enarkivigi %(task)"

#: ../libs/todoMenuItem.js:511
msgid "Are you sure?"
msgstr "Ĉu vi certas?"

#: ../libs/todoTxtManager.js:521
msgid "Cancel"
msgstr "Nuligi"

#: ../libs/openTextEditorEntry.js:21
msgid "Cannot open file"
msgstr "Ne eblas malfermi dosieron"

#: ../libs/todoTxtManager.js:511
msgid "Create new file"
msgstr "Krei novan dosieron"

#: ../libs/todoTxtManager.js:550
msgid "Create todo.txt and done.txt file in %(path)"
msgstr "Krei todo.txt kaj done.txt en %(path)"

#: ../libs/todoMenuItem.js:243
msgid "Decrease %(task) priority"
msgstr "Malpliigi prioritaton de %(task)"

#: ../libs/todoMenuItem.js:268 ../libs/todoMenuItem.js:479
msgid "Delete %(task)"
msgstr "Forigi %(task)"

#: ../libs/todoMenuItem.js:234
msgid "Edit %(task)"
msgstr "Modifi %(task)"

#: ../libs/todoTxtManager.js:811 ../libs/todoTxtManager.js:868
msgid "Error writing file"
msgstr "Eraro skribi sur dosieron"

#: ../libs/todoMenuItem.js:287
msgid "Expand %(task)"
msgstr ""

#: ../libs/todoMenuItem.js:242
msgid "Increase %(task) priority"
msgstr "Pliigi prioritaton de %(task)"

#: ../libs/todoMenuItem.js:300
msgid "Mark %(task) as done"
msgstr "Marki %(task) kiel faritan"

#: ../libs/newTaskEntry.js:16
msgid "New task…"
msgstr "Nova tasko…"

#: ../libs/todoTxtManager.js:544
msgid "No valid %(filename) file specified"
msgstr ""

#: ../libs/messageDialog.js:39
msgid "Ok"
msgstr "Bone"

#: ../libs/openPreferencesEntry.js:12
msgid "Open preferences"
msgstr "Malfermi preferojn"

#: ../libs/todoTxtManager.js:517
msgid "Open settings"
msgstr "Malfermi agordojn"

#: ../libs/openTextEditorEntry.js:14
msgid "Open todo.txt file in text editor"
msgstr ""

#: ../libs/todoTxtManager.js:525
msgid "Please choose what you want to do"
msgstr ""

#: ../libs/todoTxtManager.js:545
msgid "Select location in settings"
msgstr ""

#: ../libs/todoMenuItem.js:501
msgid "Undo delete %(task)"
msgstr ""

#: ../libs/todoTxtManager.js:389
msgid "Ungrouped"
msgstr "Sen grupo"

#: ../libs/todoTxtManager.js:820 ../libs/todoTxtManager.js:874
msgid "Unknown error during file write: %(error)"
msgstr ""

#: ../libs/todoTxtManager.js:506
msgid "Use existing file"
msgstr ""

# settings.json
msgid "Action on clicking task"
msgstr ""

# settings.json
msgid "Auto-add creation date to new tasks"
msgstr ""

# settings.json
msgid "Auto-archive done tasks"
msgstr ""

# settings.json
msgid "Behavior"
msgstr "Konduto"

# settings.json
msgid "Both"
msgstr "Ambaŭ"

# settings.json
msgid "Bottom of the file"
msgstr ""

# settings.json
msgid "Color for detected URLs"
msgstr ""

# settings.json
msgid "Confirm task deletion"
msgstr ""

# settings.json
msgid "Contexts"
msgstr "Kuntekstoj"

# settings.json
msgid "Custom color"
msgstr "Propra koloro"

# settings.json
msgid "Custom color for URLS"
msgstr ""

# settings.json
msgid "Debug"
msgstr "Sencimigo"

# settings.json
msgid "Debug level"
msgstr "Nivelo de sencimigaj informoj"

# settings.json
msgid "Debugging"
msgstr "Sencimigado"

# settings.json
msgid "Dedicated button"
msgstr ""

# settings.json
msgid "Detail"
msgstr "Detaloj"

# settings.json
msgid "Display"
msgstr "Montro"

# settings.json
msgid "Done.txt location"
msgstr "Loko de done.txt"

# settings.json
msgid "Due date extension"
msgstr ""

# settings.json
msgid "Edit"
msgstr "Redakti"

# settings.json
msgid "End"
msgstr "Fino"

# settings.json
msgid "Error"
msgstr "Eraro"

# settings.json
msgid "Extensions"
msgstr "Etendaĵoj"

# settings.json
msgid "Files"
msgstr "Dosieroj"

# settings.json
msgid "Flow"
msgstr "Fluo"

# settings.json
msgid "General"
msgstr "Ĝenerala"

# settings.json
msgid "Get color from theme"
msgstr ""

# settings.json
msgid "Group tasks by"
msgstr "Grupigi taskojn laŭ"

# settings.json
msgid "Grouping"
msgstr "Grupigo"

# settings.json
msgid "Hidden tasks extension"
msgstr ""

# settings.json
msgid "Hide if pattern is zero"
msgstr ""

# settings.json
msgid "Icon"
msgstr "Piktogramo"

# settings.json
msgid "If the due date extension is enabled, tasks containing 'due:<some date>' will be shown differently"
msgstr ""

# settings.json
msgid "If the hidden extension is enabled, tasks containing 'h:1' will not be shown"
msgstr ""

# settings.json
msgid "If the specified pattern is zero, the elements specified here will be hidden"
msgstr ""

# settings.json
msgid "If this is true, an icon will be shown in the top bar"
msgstr ""

# settings.json
msgid "If this template evaluates to zero, the top bar element will be hidden. Shortcuts still work."
msgstr ""

# settings.json
msgid "Info"
msgstr "Informoj"

# settings.json
msgid "Interface elements"
msgstr "Fasadaj elementoj"

# settings.json
msgid "Keep as is (non-standard)"
msgstr ""

# settings.json
msgid "Keep open menu after adding new task"
msgstr ""

# settings.json
msgid "Keep with pri: prefix"
msgstr ""

# settings.json
msgid "Keeps open the tasks menu after a new task is added. The new task entry will be focused."
msgstr ""

# settings.json
msgid "Keyboard shortcut to open the task list"
msgstr ""

# settings.json
msgid "Level of debug information"
msgstr ""

# settings.json
msgid "Location of the text file that contains completed (archived) tasks"
msgstr ""

# settings.json
msgid "Location of the text file that contains the tasks in todo.txt syntax"
msgstr ""

# settings.json
msgid "Location to insert new task"
msgstr ""

# settings.json
msgid "Location to truncate long tasks"
msgstr ""

# settings.json
msgid "Mark as done or archive"
msgstr ""

# settings.json
msgid "Maximum task width in pixels"
msgstr ""

# settings.json
msgid "Method to expand/contract truncated tasks"
msgstr ""

# settings.json
msgid "Middle"
msgstr "Mezo"

# settings.json
msgid "No grouping"
msgstr "Ne grupigi"

# settings.json
msgid "Nothing"
msgstr "Nenio"

# settings.json
msgid "Only show tasks with a given or higher priority. Leave empty to show all tasks."
msgstr ""

# settings.json
msgid "Only show tasks with a priority of or above"
msgstr ""

# settings.json
msgid "Open task list"
msgstr "Malfermi liston de taskoj"

# settings.json
msgid "Order elements by their priority"
msgstr ""

# settings.json
msgid "Pattern to match for zero"
msgstr ""

# settings.json
msgid "Priorities"
msgstr "Prioritatoj"

# settings.json
msgid "Priority markup"
msgstr ""

# settings.json
msgid "Priority on task completion"
msgstr ""

# settings.json
msgid "Projects"
msgstr "Projektoj"

# settings.json
msgid "Put ungrouped tasks in separate group"
msgstr ""

# settings.json
msgid "Remove"
msgstr "Forigi"

# settings.json
msgid "Same as task"
msgstr ""

# settings.json
msgid "Scroll up/down"
msgstr ""

# settings.json
msgid "Select location of done.txt file"
msgstr ""

# settings.json
msgid "Select location of todo.txt file"
msgstr ""

# settings.json
msgid "Shortcuts"
msgstr ""

# settings.json
msgid "Show 'open in text editor'"
msgstr ""

# settings.json
msgid "Show 'open preferences'"
msgstr ""

# settings.json
msgid "Show change task priority buttons"
msgstr ""

# settings.json
msgid "Show contexts"
msgstr "Montri kuntekstojn"

# settings.json
msgid "Show delete task button"
msgstr ""

# settings.json
msgid "Show done tasks"
msgstr ""

# settings.json
msgid "Show done/archive task button"
msgstr ""

# settings.json
msgid "Show edit task button"
msgstr ""

# settings.json
msgid "Show icon"
msgstr "Montri piktogramon"

# settings.json
msgid "Show new task entry"
msgstr ""

# settings.json
msgid "Show number of tasks in group"
msgstr ""

# settings.json
msgid "Show projects"
msgstr "Montri projektojn"

# settings.json
msgid "Start"
msgstr "Komenco"

# settings.json
msgid "Style priorities"
msgstr ""

# settings.json
msgid "Styles"
msgstr "Stiloj"

# settings.json
msgid "Tasks can be grouped together based on the selected property"
msgstr ""

# settings.json
msgid "Tasks that don't have the grouping priority can be put in a special 'ungrouped' group, or shown outside any groups"
msgstr ""

# settings.json
msgid "Tasks will be truncated to this width (specified in pixels) if truncating is enabled."
msgstr ""

# settings.json
msgid "Template string for display"
msgstr ""

# settings.json
msgid "Template that determines what is displayed in the top bar"
msgstr ""

# settings.json
msgid "Text"
msgstr "Teksto"

# settings.json
msgid "The action that will initiate expansion and contraction of tasks"
msgstr ""

# settings.json
msgid "The location in the task text where the ellipsization will occur"
msgstr ""

# settings.json
msgid "The number of tasks in a subgroup can be shown in the interface"
msgstr ""

# settings.json
msgid "The way that tasks with different priorities are displayed"
msgstr ""

# settings.json
msgid "This color will be used for URLs if 'Custom color' was selected above"
msgstr ""

# settings.json
msgid "Todo.txt location"
msgstr "Loko de todo.txt"

# settings.json
msgid "Top Bar"
msgstr ""

# settings.json
msgid "Top of the file"
msgstr "Komenco de la dosiero"

# settings.json
msgid "Truncate long tasks"
msgstr ""

# settings.json
msgid "Truncating"
msgstr ""

# settings.json
msgid "URL Color"
msgstr "Kolodo de retadresoj"

# settings.json
msgid "Warning"
msgstr "Averto"

# settings.json
msgid "What should be done with the priority of a task when that task is completed"
msgstr ""

# settings.json
msgid "What to do when a task is clicked"
msgstr ""

# settings.json
msgid "When URLs are detected in a task, they will be displayed in this color"
msgstr ""

# settings.json
msgid "Where in the text-file the new task will be added. Also moves insert box to the top of the interface."
msgstr ""

# settings.json
msgid "Whether a button is shown to delete a task"
msgstr ""

# settings.json
msgid "Whether a button is shown to edit a task"
msgstr ""

# settings.json
msgid "Whether a button is shown to mark active tasks as completed or to archive completed tasks, if auto-archive is off."
msgstr ""

# settings.json
msgid "Whether a confirmation dialog is shown before deleting a task"
msgstr ""

# settings.json
msgid "Whether a creation date is automatically added to newly created tasks"
msgstr ""

# settings.json
msgid "Whether a menu element is shown to open the extension's preferences"
msgstr ""

# settings.json
msgid "Whether a menu element is shown to open the todo.txt file in the default text editor"
msgstr ""

# settings.json
msgid "Whether an entry field is shown to create new tasks (new tasks can also be added by modifying the todo.txt file)"
msgstr ""

# settings.json
msgid "Whether arrows are shown to increase or decrease the task priority"
msgstr ""

# settings.json
msgid "Whether completed tasks that are not archived will be shown"
msgstr ""

# settings.json
msgid "Whether completed tasks will be automatically archived (i.e. moved to the done.txt file)"
msgstr ""

# settings.json
msgid "Whether contexts are shown in the interface (does not affect grouping)"
msgstr ""

# settings.json
msgid "Whether long tasks are truncated if they exceed a specified width"
msgstr ""

# settings.json
msgid "Whether projects are shown in the interface (does not affect grouping)"
msgstr ""

# settings.json
msgid "Whether tasks with a certain priority are shown in a specific style"
msgstr ""

# settings.json
msgid "Whether the tasks with a higher priority should be shown higher up in the list or the order should be left as in the todo.txt file"
msgstr ""
