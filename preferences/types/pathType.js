const { Gtk } = imports.gi;
const Gettext = imports.gettext.domain('todotxt');
const _ = Gettext.gettext;

const ExtensionUtils = imports.misc.extensionUtils;
const SettingsType = ExtensionUtils.getCurrentExtension().imports.preferences.types.settingsType.SettingsType;

/* exported PathType */
var PathType = class extends SettingsType {
    _selectPath(entryTarget, title, setting, settings, prefsWindow) {
        let dialogTitle = _("Select file");
        if (title !== null) {
            dialogTitle = title;
        }
        const chooser = new Gtk.FileChooserDialog({
            title: dialogTitle,
            action: Gtk.FileChooserAction.OPEN,
            modal: true,
            transient_for: prefsWindow
        });

        chooser.add_button(_("Cancel"), Gtk.ResponseType.CANCEL);
        chooser.add_button(_("Open"), Gtk.ResponseType.OK);
        chooser.set_default_response(Gtk.ResponseType.OK);
        chooser.connect('response', (dialog, response_id) => {
            if (response_id === Gtk.ResponseType.OK) {
                const file = dialog.get_file();
                if (file) {
                    entryTarget.set_subtitle(file.get_path());
                    settings.set(setting, file.get_path());
                }
            }
            dialog.destroy();
        });
        chooser.show();
    }

    get_widget() {
        return 'path';
    }

    get_row() {
        return this.get_widget();
    }

    get_signal() {
        return null;
    }

    get_value_from_widget(ignored_object) {
        return null;
    }

    update_widget(widget, setting_value) {
        widget.set_subtitle(setting_value);
    }

    extra(params) {
        params.widget.connect('activated', (ignored_object) => {
            this._selectPath(params.widget, _(params.settings.getExtraParams(params.setting)['description']),
                params.setting, params.settings, params.prefsWindow);
        });
    }
};


