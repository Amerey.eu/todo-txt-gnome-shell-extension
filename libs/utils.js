const ByteArray = imports.byteArray;
const {Gio, Gdk, Gtk} = imports.gi;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Logger = Extension.imports.third_party.logger.logger.Logger;
const Shared = Extension.imports.libs.sharedConstants;

/* exported getDefaultLogger */
function getDefaultLogger() {
    const logger = new Logger();
    logger.prefix = '[todo.txt]';
    logger.addLevel('error', '[ERROR  ]', Shared.LOG_ERROR);
    logger.addLevel('warning', '[WARNING]', Shared.LOG_WARNING);
    logger.addLevel('info', '[INFO   ]', Shared.LOG_INFO);
    logger.addLevel('detail', '[DETAIL ]', Shared.LOG_DETAIL);
    logger.addLevel('debug', '[DEBUG  ]', Shared.LOG_DEBUG);
    logger.addLevel('flow', '[FLOW   ]', Shared.LOG_FLOW);
    logger.addNewLine = false;
    return logger;
}

function isValid(object) {
    if (typeof object == 'undefined') {
        return false;
    }
    if (object === null) {
        return false;
    }
    return true;
}

/* exported isChildValid */
function isChildValid(object, child) {
    if (!isValid(object)) {
        return false;
    }
    return Object.prototype.hasOwnProperty.call(object, child) && isValid(object[child]);
}

/* exported getDefaultIfInvalid */
function getDefaultIfInvalid(object, defaultValue) {
    if (!isValid(object)) {
        return defaultValue;
    }
    return object;
}

/* exported getIconFromNames */
function getIconFromNames(names) {
    if (!isValid(names)) {
        return null;
    }
    if (!(names instanceof Array)) {
        names = [ names ];
    }
    return Gio.ThemedIcon.new_from_names(names);
}

function getDottedChild(object, string) {
    return string.split('.').reduce((accumulator, value) => {
        if (Object.prototype.hasOwnProperty.call(accumulator, value) && isValid(accumulator, value)) {
            return accumulator[value];
        }
        return null;
    }, object);
}

/* exported getFirstValidChild */
function getFirstValidChild(object, candidateChildren) {
    for (let i = 0, len = candidateChildren.length; i < len; i++) {
        if (isValid(getDottedChild(object, candidateChildren[i]))) {
            return getDottedChild(object, candidateChildren[i]);
        }
    }
    return null;
}

/* exported arrayToString */
function arrayToString(array) {
    if (array instanceof Uint8Array) {
        return ByteArray.toString(array);
    }
    return array.toString();
}

/* exported isKeyvalForbidden */
function isKeyvalForbidden(keyval) {
    const forbiddenKeyvals = [
        Gdk.KEY_Home,
        Gdk.KEY_Left,
        Gdk.KEY_Up,
        Gdk.KEY_Right,
        Gdk.KEY_Down,
        Gdk.KEY_Page_Up,
        Gdk.KEY_Page_Down,
        Gdk.KEY_End,
        Gdk.KEY_Tab,
        Gdk.KEY_KP_Enter,
        Gdk.KEY_Return,
        Gdk.KEY_Mode_switch,
    ];
    return forbiddenKeyvals.includes(keyval);
}

/* exported isBindingValid */
function isBindingValid({ mask, keycode, keyval }) {
    if ((mask === 0 || mask === Gdk.SHIFT_MASK) && keycode !== 0) { //eslint-disable-line no-magic-numbers
        if (
            (keyval >= Gdk.KEY_a && keyval <= Gdk.KEY_z) ||
            (keyval >= Gdk.KEY_A && keyval <= Gdk.KEY_Z) ||
            (keyval >= Gdk.KEY_0 && keyval <= Gdk.KEY_9) ||
            (keyval >= Gdk.KEY_kana_fullstop && keyval <= Gdk.KEY_semivoicedsound) ||
            (keyval >= Gdk.KEY_Arabic_comma && keyval <= Gdk.KEY_Arabic_sukun) ||
            (keyval >= Gdk.KEY_Serbian_dje && keyval <= Gdk.KEY_Cyrillic_HARDSIGN) ||
            (keyval >= Gdk.KEY_Greek_ALPHAaccent && keyval <= Gdk.KEY_Greek_omega) ||
            (keyval >= Gdk.KEY_hebrew_doublelowline && keyval <= Gdk.KEY_hebrew_taf) ||
            (keyval >= Gdk.KEY_Thai_kokai && keyval <= Gdk.KEY_Thai_lekkao) ||
            (keyval >= Gdk.KEY_Hangul_Kiyeog && keyval <= Gdk.KEY_Hangul_J_YeorinHieuh) ||
            (keyval === Gdk.KEY_space && mask === 0) || //eslint-disable-line no-magic-numbers
            isKeyvalForbidden(keyval)
        ) {
            return false;
        }
    }
    return true;
}

/* exported isAccelValid */
function isAccelValid({ mask, keyval }) {
    //eslint-disable-next-line no-magic-numbers
    return Gtk.accelerator_valid(keyval, mask) || (keyval === Gdk.KEY_Tab && mask !== 0);
}


/* vi: set expandtab tabstop=4 shiftwidth=4: */
